# Formation React Redux

[Diaporama](https://zarchinard.bitbucket.io/formation-react-redux.html)

---

## Javascript

Créer un fichier avec l'extension **.js** et l'ouvrir dans VS Code.  
Installer l'extension `Code Runner` pour exécuter le contenu du fichier.  

### 1. Création d'objet

#### 1.1. Constructeur

Créer un constructeur **Entreprise** avec le mot-clé `class` qui prend en entrée un tableau de valeurs et retourne un objet avec les bonnes propriétés.

*Exemple*  
entrée  
```javascript
let entree = ["Umbrella Corporation", 1234567890, "01/01/1900", 10000, 1.05];
```  
sortie  
```javascript
{
    nom: "Umbrella Corporation",
    siren: 1234567890,
    dateCreation: "01/01/1900",
    capital: 10000,
    tauxCroissance: 1.05
}
```
Tester le constructeur avec l'exemple ci-dessus.

#### 1.2. Utilisation de Moment.js

Télécharger [Moment.js](https://momentjs.com/downloads/moment-with-locales.min.js), placer le fichier dans le même dossier que le fichier .js du tp. Dans le fichier du tp, importer Moment.js avec `const moment = require("./moment-with-locales.min.js");`.  

Modifier le constructeur pour que la propriété `dateCreation` ne soit pas un `string` mais un objet moment.

### 2. Manipuler des tableaux

#### 2.1 Créer un nouveau tableau à partir d'un autre

A partir du tableau d'entrées suivant, créer un tableau de sorties contenant des objets `Entreprise`.

*entrée*
```javascript
const entrees = [["Umbrella Corporation", 1234567890, "01/01/1900", 10000, 1],
                ["Moebius Foundation", 4592642628, "30/05/2050", 1000000, 1.002],
                ["Beau-Line", 5481369702, "25/08/1990", 750000, 1.03],
                ["Red Apple", 2469325170, "12/02/1927", 20000, 1.02],
                ["Pizza Planet", 5064923153, "05/07/1948", 500000, 1.01],
                ["Lemming Brothers", 613458012, "06/01/2000", 999999, 1.07],
                ["Los Pollos Hermanos", 4201698267, "18/03/1981", 250000, 1.1],
                ["Krusty Burger", 3160584290, "23/10/1960", 1000, 10],
                ["Liberty Tree", 3064192517, "10/12/1978", 600000, 1.001],
                ["E Corp", 5986201746, "06/04/2005", 1000000, 1.05]];
```

On peut utiliser une boucle `for`, ou la méthode `map`, ou encore [Underscore.js](https://underscorejs.org/underscore-min.js) qu'on importe de la même manière que moment, avec `const _ = require("./underscore-min.js");`.

#### 2.2 Trier un tableau

Trier le tableau de sorties selon le `nom` :

* Avec la méthode `sort` en natif, pour comparer deux chaînes de caractères : `s1.localeCompare(s2)`
* Avec la méthode `sortBy` d'underscore

#### 2.3 Filtrer les données

Récupérer les entreprises qui ont été créées avant 1980 avec `filter`.

### 3. FACULTATIF Croissance des entreprises

#### 3.1 Méthode de croissance

Créer une fonction `croitre` qui met à jour le capital d'une entreprise, le nouveau capital correspond à l'ancien multiplié par le taux de croissance.

#### 3.2 Sélection aléatoire et clone des objets

Sélectionner aléatoirement deux entreprises dans le tableau de sorties à l'aide de la fonction `Math.random()`, la fonction `Math.floor(nombre)` permet d'obtenir la partie entière d'un nombre.

Cloner ces deux entreprises avec la méthode `_.clone(objet)`. Par la suite on travaille sur les clones.

#### 3.3 Comparer la croissance de deux entreprises

Faire croître les deux entreprises à chaque seconde jusqu'au moment où l'une des deux atteint 5 millions en capital, afficher le nom de cette entreprise ainsi que le nombre de secondes écoulées.

*Indice*
```javascript
let idInterval = setInterval(function(){
    // fait croître les entreprises si la condition d'arrêt n'est pas atteinte
    // si c'est ok, on arrête l'interval avec clearInterval(idInterval)
}, 1000);
```

---

## React

### 1. Commencer un projet react

#### 1.1 Installation de Node.js

Si on dispose des droits administrateurs, on peut installer Node.js normalement.

Sinon, il faut télécharger une version portable de Node.js et indiquer au système son emplacement : ajouter à la variable d'environnement `path` le chemin vers le dossier contenant `node.exe`.

Ouvrir un terminal ou invite de commande et taper `npm` pour vérifier que node est bien installé.  
Paramétrer le proxy pour npm avec la commande suivante :
```
npm config set https-proxy http://proxy-rie.http.insee.fr:8080
```

#### 1.2 Utiliser create-react-app

Se positionner dans un dossier où on souhaite créer les projets, exécuter `create-react-app` à l'aide de `npx` pour créer un projet.
```
D:\idep\workspace> npx create-react-app mon-projet
```

Vérifier qu'un dossier mon-projet a été créé, lancer l'application avec la commande `npm start` dans ce dossier.

#### 1.3 Un élément react

Ouvrir le dossier du projet depuis Visual Studio Code, modifier le point d'entrée  `src/index.js` pour que l'application affiche "Bonjour invité !" dans une balise h1 plutôt que le contenu par défaut.

*Note : parcourir le dossier du projet*

### 2. Composants

Récupérer le projet *formation-react-redux* depuis le dépôt git, se placer sur la branche *tp-react-2*. Lancer la ligne de commande `npm install` pour installer les modules nécessaires, puis `npm start` pour lancer l'application.

#### 2.1 Créer un composant

Dans le dossier **src**, créer un fichier *Accueil.js*, écrire le composant `Accueil` qui renvoie un titre (h1) dont le contenu est "Bonjour invité !".

Ne pas oublier d'importer `React` dans le fichier js mais aussi d'exporter par défaut le composant `Accueil`.

#### 2.2 Composant avec props

De la même manière, créer le composant `Entreprise` dont le `props.entreprise` est un objet Entreprise. Ce composant affiche les informations de l'entreprise :

*entreprise*
```javascript
{nom: "Umbrella Corporation",
siren: 1234567890,
dateCreation: [1900, 1, 1],
capital: 10000,
statut: "SARL",
domaines: ["Recherche"]}
```
*résultat*
```
Umbrella Corporation [1234567890]
Date de création : 01/01/1900
Statut : SARL
Domaine(s) : Recherche
Capital : 10000
```

#### 2.3 Intégrer les composants à l'application

Dans *App.js*, importer les composants créés et les intégrer dans le composant `App`.

### 3. Gestion d'état

Se placer sur la branche *tp-react-3*.

#### 3.1 Ajouter un état à un composant

Dans le composant `Entreprise`, créer le constructeur et ajouter à l'état la propriété `afficherDetails` qui sera `false` par défaut.

#### 3.2 Gestion d'événements

Créer les méthodes `afficherDetail()` et `cacherDetails()` qui vont respectivement modifier la valeur de `afficherDetails` à `true` et `false`.

Ajouter les événements `onMouseEnter` et `onMouseLeave` à l'élément `<div>` et leur affecter la bonne méthode.

#### 3.3 Affichage conditionnel

Modifier la méthode `render` : lorsque `afficherDetails` est `true`, elle retourne toutes les informations de l'entreprise, sinon elle retourne uniquement la première ligne (nom et siren).

### 4. Appel à un web service

Se placer sur la branche *tp-react-4*.

#### 4.1 Liste de composants

Créer un composant `Entreprises` qui retourne un tableau de composants `Entreprise`, les données étant stockées dans l'état du composant. A l'état initial, il y a 0 entreprise.

#### 4.2 Récupérer les données

Ajouter la méthode `useEffect` qui permet de récupérer la liste d'entreprises depuis le web service fourni.

Dans le composant `App`, remplacer le composant `Entreprise` par `Entreprises`.

#### 4.3 Externaliser l'url du web service

Dans le dossier `/public` du projet, créer le fichier **configuration.json** dans lequel ajouter le contenu suivant :  
```json
{
    "api_url": "url fourni pendant la formation"
}
```

Dans le code faire deux `fetch` imbriqués, le premier récupère l'url de l'api, le deuxième utilise l'url récupérée pour faire la vraie requête.
```javascript
fetch("/configuration.json").then(responseJson => responseJson.json().then(
    fichierJson => {
        fetch(fichierJson.api_url + "/ma-requete").then( /*...*/ )
    }
))
```

Cette étape permet de ne pas écrire en dur l'adresse du web service, sur un serveur de production, on surcharge le contenu du fichier configuration au déploiement.

### 5. Partage d'états entre les composants

Se placer sur la branche *tp-react-5*.

#### 5.1 Modifier l'état du composant parent

Dans le composant `App`, ajouter à l'état la propriété `contenu` dont la valeur par défaut est "accueil". Modifier la méthode `render` du composant, elle renvoie toujours `Menu` mais selon la valeur de `contenu`, elle renvoie soit `Accueil` soit `Entreprises`.   
Créer la méthode `changerContenu(arg)` qui change la valeur de `contenu` avec l'argument. Passer cette méthode dans le props du composant `Menu`.

Dans le composant `Menu`, ajouter des événements `onClick` aux \<div\> appropriées pour déclencher le changement de contenu pour le composant `App`.

**Attention** : dans une expression JSX, la propriété `onClick` prend comme valeur une fonction. Lorsqu'on veut associer l'événement avec l'exécution d'une fonction avec un paramètre donné, on crée une nouvelle fonction.
```javascript
const uneFonction = function(arg) {/* traitement avec arg*/};
<button onClick = {() => uneFonction(arg1)}>arg1</button>
```

Dans les composants `Accueil` et `Entreprises`, ajouter à la méthode `componentDidMount` la ligne suivante pour modifier le titre de la page.
```javascript
document.title = "Page d'accueil" // ou "Liste d'entreprises"
```

#### 5.2 Partage d'état

On souhaite afficher dans le composant `Accueil` le nombre d'entreprises, pour cela le composant doit aussi connaître la liste d'entreprises.  
Modifier `App` et `Entreprises` pour que la liste d'entreprises soit gérée par `App`, qui passera les informations nécessaires dans les props de ses enfants.

#### 5.3 Ajouter une entreprise

Compléter le composant `AjouterEntreprise` contenant le formulaire qui permet d'ajouter une entreprise, il n'y a pas besoin de gérer la validation pour cet exercice.  
Ajouter un item au menu pour afficher ce composant.

Lorsque la requête a réussi, il déclenche la mise à jour de la liste des entreprises au niveau de `App`, affiche un message d'information en bas du formulaire et réinitialise le formulaire. Dans le cas contraire, il affiche l'erreur en bas du formulaire.

---

## Redux

### 1. Mise en place de Redux

**Objectif** : créer le store redux qui stock l'état `contenu` porté par `App`  
Se placer sur la branche *tp-redux-1*  
Installer redux avec la commande `npm install redux react-redux`

#### 1.1 Action

Dans le dossier *src*, créer un sous-dossier *actions*, à l'intérieur créer un fichier *pageAction.js*  
Définir un type d'action `CHANGER_PAGE` et la fonction créatrice d'actions associée `changerPageAction`
```jsx
changerPageAction("accueil") // retournera {type: CHANGER_PAGE, page: "accueil"}
```

#### 1.2 Reducer

Dans le dossier *src*, créer un sous-dossier *reducers*, à l'intérieur créer un fichier *pageReducer.js*  
Créer le reducer `pageReducer` qui retourne un nouvel état à partir de l'action, l'état initial étant "accueil"

Dans le même dossier, créer le fichier *index.js* dans lequel on définira le reducer global.

#### 1.3 Store

Modifier le fichier *src/index.js* qui est la racine du projet pour créer le store et placer le composant `App` à l'intérieur du `Provider`

Installer PropTypes : `npm install prop-types`  
Dans le dossier *src*, créer un sous-dossier *components*, déplacer *Menu.js* dans ce dossier et créer le fichier *MenuContainer.js* qui va connecter `Menu` au store pour modifier le state page. Ajouter les proptypes pour `Menu`.

Modifier `App` pour qu'il utilise `MenuContainer` à la place de `Menu`, puis connecter `App` au store afin qu'il récupère le state redux et affiche la bonne page. Le state du composant ne devrait plus avoir la propriété "page".

### 2. Actions asynchrones

Se placer sur la branche *tp-redux-2*  
Installer redux-thunk : `npm install redux-thunk`

#### 2.1 Récupérer la liste des entreprises avec redux

Créer le fichier *src/actions/listeEntreprisesAction.js*, y définir :

* Les types d'actions : REQUETE_ENTREPRISES, SUCCES_ENTREPRISES, ERREUR_ENTREPRISES
* Les fonctions créatrices associées
* La fonction `recupereEntreprisesAction` qui renvoie l'action asynchrone

Créer le fichier *src/reducers/listeEntreprisesReducer.js* pour définir le reducer associé, ajouter ce reducer au reducer global.

Modifier le store pour qu'il utilise redux-thunk.

Réorganiser les composants :

* déplacer Accueil.js dans *src/components*, ajouter ses propTypes et créer *AccueilContainer.js* qui connecte `Accueil` au store
* faire la même chose avec `Entreprises`, on ne modifiera pas `Entreprise`
* Modifier `App` pour qu'il soit sans état et utilise `AccueilContainer` et `EntreprisesContainer`

#### 2.2 Ajouter une entreprise avec redux

Créer le fichier *src/actions/ajouterEntrepriseAction.js*, y définir :

* Les types d'actions : REQUETE_AJOUT_ENTREPRISE, SUCCES_AJOUT, ERREUR_AJOUT
* Les fonctions créatrices associées
* La fonction `ajouterEntrepriseAction` qui renvoie l'action asynchrone

Créer le fichier *src/reducers/ajouterEntrepriseReducer.js* pour définir le reducer associé : son état possède les propriétés chargement(boolean), succes(boolean), error  
Ajouter ce reducer au reducer global

Déplacer *AjouterEntreprise.js* dans le dossier *components*, créer le fichier *AjouterEntrepriseContainer.js* qui va connecter le composant au store pour avoir une fonction qui va ajouter l'entreprise, une fonction qui actualise la liste d'entreprises du store et le message affiché en bas du formulaire (error si elle n'est pas null, "Création réussie" si succes vaut true, et rien sinon).  
Ajouter les propTypes pour `AjouterEntreprise`, utiliser la propriété *message* du props au lieu du state. Dans la méthode `handleSubmit` utiliser la fonction de props pour ajouter une entreprise. Avec la méthode `componentDidUpdate`, réinitialiser le formulaire et actualiser la liste des entreprises si la propriété *message* a changé et vaut "Création réussie".  
```jsx
componentDidUpdate(prevProps){
    if(prevProps.message !== this.props.message && this.props.message === "Création réussie") //...
}
```
Modifier `App` pour qu'il utilise `AjouterEntrepriseContainer`

**Remarques** :

* On pourrait faire un dispatch de l'action qui récupère l'ensemble des entreprises juste après le succès du fetch d'ajout
* L'état du formulaire pourrait aussi être stocké dans le store redux plutôt que d'être porté par le component

*Question supplémentaire*   
Après avoir ajouté une entreprise, cliquer sur "Accueil" puis revenir sur "Ajouter une entreprise", que constate-t-on ?  
Ajouter le type d'action REINITIALISER_AJOUT_ENTREPRISE, la fonction créatrice et adapter le reducer pour réinitialiser l'état d'ajout d'entreprise lorsqu'on "quitte" le composant (`componentWillUnmount`)

---

## Quelques outils

### 1. React router

Se placer sur la branche *tp-react-router*  
Installer react router : `npm install react-router-dom`

#### 1.1 Mise en place

Modifier le composant `App` de sorte qu'il affiche :

* toujours le composant `Menu`
* le composant `Accueil` pour l'url "/"
* le composant `Entreprises` pour l'url "/entreprises"
* le composant `AjouterEntreprise` pour l'url "/ajouter-entreprise"
* `Accueil` (ou une page 404) pour tout autre url

#### 1.2 Modification de Menu

Supprimer l'action et le reducer `page` qui ne servent plus, mettre à jour le reducer global

Adapter les composants `App` et `Menu` pour qu'ils n'utilisent plus le state `page`  
`MenuContainer` est inutile pour le moment mais on va le conserver pour plus tard

Utiliser le composant `Link` pour le menu

### 2. Keycloak

Le web service rejette la requête d'ajout d'entreprise lorsque l'utilisateur n'est pas authentifié.  
Se placer sur la branche *tp-keycloak*  
Installer keycloak : `npm install keycloak-js`

#### 2.1 Mise en place

Dans le dossier *public*, créer le fichier keycloak.json avec les informations suivantes :  
```
{
    "auth-server-url": "https://auth.insee.test/auth",
    "realm": "agents-insee-interne",
    "resource": "localhost-frontend"
}
```
Définir dans *src/actions/keycloak.js* le type d'action MAJ_KEYCLOAK et la fonction créatrice associée pour mettre l'objet keycloak dans le store. Créer le reducer *src/reducers/keycloak.js* qui retourne par défaut l'objet `{ authenticated: false }`. Ajouter le reducer keycloak au reducer global.

Pour accéder au store et donc à l'objet keycloak depuis les fonctions créatrices d'action, on pensera à exporter le store lors de sa création dans le fichier "src/index.js".

#### 2.2 Utilisation basique

Initialiser l'objet keycloak sans forcer l'authentification dans la méthode `componentDidMount` de App et stocker l'objet obtenu dans le store.

Ajouter un bouton "Se connecter" dans le composant `Menu` lorsque l'utilisateur n'est pas authentifié et permet de se connecter, il devient "Se déconnecter" lorsque l'utilisateur est authentifié et permet de se déconnecter.

Modifier le composant `Accueil` qui affiche le nom de l'utilisateur s'il est connecté, "invité" sinon.

Mettre à jour éventuellement les propTypes des composants `Menu` et `Accueil`.

#### 2.3 Gestion des droits d'accès

Dans le menu, afficher le bouton "Ajouter une entreprise" uniquement si l'utilisateur est connecté.

En tant que invité, on peut toujours accéder au formulaire d'ajout d'entreprise en tapant l'url dans la barre d'adresse. S'inspirer de l'exemple [ici](https://reacttraining.com/react-router/web/example/auth-workflow) pour créer le composant `PrivateRoute` qui affiche le composant demandé si l'utilisateur est authentifié, un message d'erreur sinon. Utiliser cette PrivateRoute pour `AjouterEntreprise`.

Modifier la requête dans *src/actions/ajouterEntreprise.js* : le token keycloak doit être rafraîchi puis ajouté dans les headers de la requête.