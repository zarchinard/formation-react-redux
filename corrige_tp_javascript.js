// 1. Cr�ation d'objet
class Entreprise {
    constructor(tableau){
        this.nom = tableau[0];
        this.siren = tableau[1];
        this.dateCreation = moment(tableau[2], "DD-MM-YYYY"); //moment
        this.capital = tableau[3];
        this.tauxCroissance = tableau[4];
    }
}

let entree = ["Umbrella Corporation", 1234567890, "01/01/1900", 10000, 1.05];
let sortie = new Entreprise(entree);
console.log(sortie);

// 2. Manipuler des tableaux
const entrees = [["Umbrella Corporation", 1234567890, "01/01/1900", 10000, 1],
                ["Moebius Foundation", 4592642628, "30/05/2050", 1000000, 1.002],
                ["Beau-Line", 5481369702, "25/08/1990", 750000, 1.03],
                ["Red Apple", 2469325170, "12/02/1927", 20000, 1.02],
                ["Pizza Planet", 5064923153, "05/07/1948", 500000, 1.01],
                ["Lemming Brothers", 613458012, "06/01/2000", 999999, 1.07],
                ["Los Pollos Hermanos", 4201698267, "18/03/1981", 250000, 1.1],
                ["Krusty Burger", 3160584290, "23/10/1960", 1000, 10],
                ["Liberty Tree", 3064192517, "10/12/1978", 600000, 1.001],
                ["E Corp", 5986201746, "06/04/2005", 1000000, 1.05]];

const avecBoucleFor = [];
for(i = 0; i < entrees.length; i++){
    avecBoucleFor[i] = new Entreprise(entrees[i]);
}
console.log('tableau de sorties avec for :');
console.log(avecBoucleFor);

const avecMap = entrees.map(e => new Entreprise(e));
console.log('tableau de sorties avec map :');
console.log(avecMap);

const avecUnderscore = _.map(entrees, e => new Entreprise(e));
console.log('tableau de sorties avec _ :');
console.log(avecUnderscore);

const sorties = entrees.map(e => new Entreprise(e));
// en natif
sorties.sort((e1, e2)=> e1.nom.localeCompare(e2.nom));
console.log('tableau tri� avec sort :');
console.log(sorties)
// avec underscore qui cr�e une copie du tableau
console.log('tableau tri� avec _ :');
console.log(_.sortBy(sorties, 'nom'));

let avant1980 = sorties.filter(e => e.dateCreation.isBefore(moment("01-01-1980", "DD-MM-YYYY")));
console.log('cr��es avant 1980 :');
console.log(avant1980);

// 3. Croissance des entreprises
let indice1 = Math.floor(Math.random()*sorties.length);
let indice2 = Math.floor(Math.random()*sorties.length);
while(indice2 === indice1){
    indice2 = Math.floor(Math.random()*sorties.length);
}

let entreprise1 = _.clone(sorties[indice1]);
let entreprise2 = _.clone(sorties[indice2]);
let nombreDeSecondesEcoulees = 0;

const croitre = function(){
    console.log("croissance de "+ this.nom + " dont le capital vaut "+ this.capital);
    this.capital = this.capital * this.tauxCroissance;
}

let idInterval = setInterval(() => {
      if (entreprise1.capital < 5000000 && entreprise2.capital < 5000000){
          croitre.call(entreprise1);
          croitre.call(entreprise2);
          nombreDeSecondesEcoulees++;
      }
      else {
        clearInterval(idInterval);
        if(entreprise1.capital >= 5000000) console.log(entreprise1.nom);
        if(entreprise2.capital >= 5000000) console.log(entreprise2.nom);
        console.log(nombreDeSecondesEcoulees);
      }
    }, 1000);